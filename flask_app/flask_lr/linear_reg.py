import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LinearRegression
from sklearn.metrics import r2_score
import matplotlib
matplotlib.use('TkAgg')
import matplotlib.pyplot as plt
import seaborn as sns


df=pd.read_csv("../weatherHistory.csv")

df2=df[['Humidity','Temperature (C)']]

print(df2.isnull().values.any())

train_set, test_set = train_test_split(df2, test_size=0.2, random_state=42)

df_copy = train_set.copy()

# print(df_copy.corr())
#
# df_copy.plot.scatter(x='Humidity', y='Temperature (C)')

train_set_full=train_set.copy()


train_set = train_set.drop(["Humidity"], axis=1)

test_set_full=test_set.copy()

test_set=test_set.drop(["Humidity"], axis=1)

train_labels = df_copy["Humidity"]

lin_reg = LinearRegression().fit(train_set, train_labels)
#
# print("Coefficients: ", lin_reg.coef_)
# print("Intercept: ", lin_reg.intercept_)

# temp = np.zeros(1)
# temp[0] = 9.472222222222221
# salary_pred = lin_reg.predict(temp.reshape(1, -1))
#
# print("Humidity:", salary_pred)
# salary_pred = lin_reg.predict(test_set)
# #
# print(test_set_full,"Temperature:", test_set, "Humidity:", salary_pred)
# print(test_set_full["Humidity"])
# print(lin_reg.score(test_set, test_set_full["Humidity"]))
# print (r2_score(test_set_full["Humidity"], salary_pred))
# # sns.regplot('Humidity', # Horizontal axis
# #            'Temperature (C)', # Vertical axis
# #            data=df_copy)
# #
# plt.scatter(test_set_full["Temperature (C)"], test_set_full["Humidity"],  color='blue')
# plt.plot(test_set_full["Temperature (C)"], salary_pred, color='red', linewidth=2)
#
# plt.show()
