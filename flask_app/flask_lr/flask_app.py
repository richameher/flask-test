from flask import Flask
from flask import jsonify
from sklearn.externals import joblib
from linear_reg import lin_reg
from flask import request
from flask import render_template
import numpy as np
import csv

app = Flask(__name__)


# Get the number

@app.route("/single_value")
def get_val():
    return render_template('form.html')


# Enter the number

@app.route("/single_value", methods=['POST'])
def predict():
    text = request.form['text']

    # Currently on for single number
    temp = np.zeros(1)
    temperature = float(text)
    temp[0] = temperature

    lin_reg1 = joblib.load("./linear_regression_model.pkl")

    # Use reshape (1,-1) as per predict functions req

    ret_val = lin_reg1.predict(temp.reshape(-1, 1)).tolist()

    return render_template('chart.html', values=temp, labels=ret_val)


# Get the form for file

@app.route("/file_upload")
def file_upload():
    return render_template('upload_csv_form.html')


# Upload the file

@app.route('/file_read', methods=["POST"])
def read_file_test():
    f = request.files['data_file']
    read_temp = csv.reader(f, delimiter=',', quotechar='|')
    for row in read_temp:
        print row[0]
    if not f:
        return "No file"
    else:
        return "Uploaded file"


@app.route('/file_upload', methods=["POST"])
def read_file():
    f = request.files['data_file']
    list_temp = []
    list_hum=[]
    read_temp = csv.reader(f, delimiter=',', quotechar='|')

    f.readline()
    for row in read_temp:
        list_temp.append(row[0])
        # list_hum.append(row[5])
    if not f:
        return "No file"

    else:
        new_arr=np.asarray(list_temp).astype(np.float)

        lin_reg1 = joblib.load("./linear_regression_model.pkl")

        ret_val = lin_reg1.predict(new_arr.reshape(-1, 1)).tolist()

        # return jsonify(ret_val)

        return render_template('chart.html', values=new_arr, labels=ret_val)


# @app.route("/predict", methods=['GET', 'POST'])
# def predicts():
#     temp = np.zeros(1)
#     if request.method == 'POST':
#         try:
#             data = request.get_json()
#             temperature = float(data["Temperature (C)"])
#
#             temp[0] = temperature
#
#             lin_reg1 = joblib.load("./linear_regression_model.pkl")
#
#         except ValueError:
#             return jsonify("Please enter a number.")
#
#         return jsonify(lin_reg1.predict(temp.reshape(1, -1)).tolist())
#     else:
#         return "Getting data"
#
#
# @app.route("/chart")
# def chart():
#     labels = ["January", "February", "March", "April", "May", "June", "July", "August"]
#     values = [10, 9, 8, 7, 6, 4, 7, 8]
#     return render_template('chart.html', values=values, labels=labels)


if __name__ == '__main__':
    joblib.dump(lin_reg, "linear_regression_model.pkl")
    app.run(debug=True)
